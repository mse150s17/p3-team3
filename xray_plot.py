import matplotlib.pyplot as plt
import numpy as np
import sys

#This program plots the square root of the frequency verses the atomic number of different samples of elements and determines plancks constant.

#To run this file use $python xray_plot.py

#defines the source of the required data and points to the column containing the frequency data.
data = np.genfromtxt('xray.txt', delimiter=None, dtype = float, skip_header = 1, usecols = 2) 

speed_of_light = 3E17 #nm/s  
freq = np.divide(speed_of_light, data)

#Units for frequency: 1/s
#Units for wavelength: nm 

sqrtfreq = np.sqrt(freq) #Takes the square root of the frequency.

#Defines source and points to the column containing the atomic number data.
atomic_num = np.genfromtxt('xray.txt', delimiter=None, dtype = float, skip_header = 1, usecols = 1)

plancks_real = 6.626E-34 #J/s
plancks_calc = ((2.17896e-18*3*(atomic_num-1)**2)/4)/freq

#Units for Planck's Constant = J*s 

avg_plancks_calc = np.mean(plancks_calc)
percent_error = ((avg_plancks_calc - plancks_real)/(plancks_real))*100
print('Percent Error = ', percent_error, '%')

print ("Planck's Constant:", avg_plancks_calc)

#pulls the slope and intercept values from our graph.
coefficents = np.polyfit(atomic_num, sqrtfreq, 1)
slope = coefficents[0]
intercept = coefficents[1]
print('B:', slope)
#print('intercept:', intercept)

C = intercept/slope
print('C:',C)

plt.plot(atomic_num, sqrtfreq)
plt.title('Square Root of Frequency vs. Atomic Number',color="green")
plt.xlabel('Atomic Number')
plt.ylabel('Square Root of Frequency')
plt.grid(True) 
plt.show()
