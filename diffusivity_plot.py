#This program is designed to determine the activation energy and diffusivity constant from a set of data. 

#Importing different librarys need to perform the operations
import matplotlib.pyplot as plt
import numpy as np
import sys
import sympy as sp

#To run this file use $python diffusivity_plot.py

data = np.genfromtxt('diffusivity.txt', delimiter=' ', dtype=float, skip_header = 1) 

a = [colomn[0] for colomn in data] 
b = [colomn[1] for colomn in data] 

inversetemp = np.reciprocal(b)
diff = np.log(a)

#polyfit gets the values of the coefficents of the ploynomials from the liniar equation defined by our graph 
coefficents = np.polyfit(inversetemp, diff, 1)
#print (coefficents) 
slope = coefficents[0]
intercept = coefficents[1]

print('Slope:', slope)
print('Diffusivity Constant (A):',np.e**intercept)

k = 1.38e-19 

print('Activation Energy (J):', slope * k) 
print('Activation Energy (eV):', (slope * k)/1.602E-19) 

#Plots the data to a graph
plt.plot(inversetemp, diff)
plt.title('Diffusivity: ln(D) vs 1/T',color="green")
plt.xlabel('Inverse Temperature (1/K)')
plt.ylabel('ln(Diffusivity)')
plt.grid(True) 
plt.show()
