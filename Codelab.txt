To run this program in codelab:

1. First, all files must be downloaded from this URL: https://bitbucket.org/mse150s17/p3-team3/downloads/
    * Press on the link titled 'Download repository.'
    * You must also unzip this directory. 

2. Once downloaded, all files must then be uploaded to codelab at the home page.
    * Go to the URL: http://codelab.boisestate.edu/user/6igiNrKqaH9w/tree
    * Press the 'upload' button and navigate to the files you just downloaded and unzipped. 
    * Select all of the files you want to work with and then click 'upload' next to each filename. 

3. Now, in the top right corner press 'New' and then go down to 'Python 3' to start a new notebook. 
    * To load the files you want to use, type the command: %load FILENAME
    * To run this press SHIFT+ENTER.

4. To run this file you just uploaded, select the window where the code just appeared. 
    * Once selected, press SHIFT+ENTER to run. 
    * It might take a while since some of the packages have to be built. 

