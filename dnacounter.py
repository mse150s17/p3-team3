
import numpy as np
import scipy
from scipy import ndimage
from matplotlib import pyplot as plt


# Read image
dna = scipy.misc.imread('dna.tif')

# Save image
plt.imsave('dna.tif',dna)

# Using a gaussian filter on the image
dna = ndimage.gaussian_filter(dna, 4.7)
dna[dna>105]=0

# Count/print number of  objects
labeled, nr_objects = ndimage.label(dna)
print ("Number of objects is %d " %(nr_objects),)
print (nr_objects)

# Show filtered  image
plt.imshow(dna)

plt.show()








