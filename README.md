The goal of this project is to fit data with polynomials, to be able to manipulate matrices, and perform integration and differentiation of mathematical functions. During this four-week project, additional challenges will be added to the p3-startrepository in response to class progress as a whole. Deliverables: A 2-page report summarizing your contributions to the project, submitted to blackboard. 

# What is this repository for? #
This repository is a set of challenges that were set forth by Dr. Jankowski for the members of Team 3 to practice their coding, and collaborating. These challenges helped this group use their python skills specifically looking at numpy, matplotlib, and sympy. 

### Challenge 1:
The objective of this challenge is to generate a plot from the measurements of diffusion of nickel in MgO. You can look at the natural log of diffusivity versus inverse temperature. It should represent this equation: 

![CodeCogsEqn.gif](https://bitbucket.org/repo/rpKGA6M/images/2793531151-CodeCogsEqn.gif)

where A is constant, E is activation energy, k is Boltzmann's constant, and T is temperature in Kelvin. 

### Challenge 2:
Given some data about the x-rays that can be emitted from various elements the code plots the square root of the light emitted by these elements vs their atomic number. Using linear fit, one can then determine the constants B and C from the equation:

![equation 1.gif] (https://bytebucket.org/snippets/mse150s17/n7K5o/raw/629523b5c3141a1d1893c0da0d7d692093b66cea/equation%201.gif) 

Using this relationship: 

![equation 2.gif] (https://bytebucket.org/snippets/kodyking/eLKn8/raw/9d6d610415709a9f17f5441dfac4407853be04bc/equation%202.gif) 

Where h is plank's constant, one can compare the real value for Planck's Constant to the experimental one. 

### Challenge 3:
A repository of instructions for how to solve Challenge 1 using codelab.boisestate.edu. Someone who has zero programming experience should be able to follow these instructions and walk through the steps to get answers for Challenge 1.


### Challenge 4:
This challenge is to write programs to calculate the average and standard deviation of the grain sizes in the grain.png file, and find the number of DNA origami tiles in the dna.tif file.


# How do I get set up? #
 * You will need to do a git clone of the SSH link: git@bitbucket.org:mse150s17/p3-team3.git using the command: $git clone git@bitbucket.org:mse150s17/p3-team3.git
 * You will need to have Python Anacaonda 3.6.0 on your computer. Python Anaconda 2.7.0 lacks sympy and will not plot Challenge 2.

# How do I run the code? #

### Challenge 1:
In a terminal on your computer make sure you are in the p3-team3 repository, and run the command: $python diffusivity_plot.py.

### Challenge 2:
In a terminal on your computer make sure you are in the p3-team3 repository, and run the command: $python xray_plot.py

### Challenge 3:
To run this program in codelab:

1. First all files must be downloaded from this URL: https://bitbucket.org/mse150s17/p3-team3/downloads/
    * Press on the link titled 'Download repository.'
    * You must also unzip this directory. 

2. Once downloaded, all files must then be uploaded to codelab at the home page.
    * Go to the URL: http://codelab.boisestate.edu/user/6igiNrKqaH9w/tree
    * Press the 'upload' button and navigate to the files you just downloaded and unzipped. 
    * Select all of the files you want to work with and then click 'upload' next to each filename. 

3. Now, in the top right corner press 'New' and then go down to 'Python 3' to start a new notebook. 
    * To load the files you want to use, type the command: %load FILENAME
    * To run this press SHIFT+ENTER.

4. To run this file you just uploaded, select the window where the code just appeared. 
    * Once selected, press SHIFT+ENTER to run. 
    * It might take a while since some of the packages have to be built. 

>This information is also available in Codelab.txt 

### Challenge 4:
Depending on the desired task, run one of the following commands:

* In a terminal on your computer make sure you are in the p3-team3 repository, and run the command: $python size.py

* In a terminal on your computer make sure you are in the p3-team3 repository, and run the command: $python grain.py

* In a terminal on your computer make sure you are in the p3-team3 repository, and run the command: $python dnacounter.py

# What should I expect? #

### Challenge 1:
Slope and intercept values should be printed in the terminal, and a graph of the line should open revealing the natural log of diffusivity of Ni in MgO over the inverse of temperature.

### Challenge 2
Slope and intercept values should be printed in the terminal, and a graph of the line should open revealing the square root of wavelength over the atomic number. 

### Challenge 3
Slope and intercept values should be printed in codelab, and a graph of the line should open revealing the natural log of diffusivity of Ni in MgO over the inverse of tempterature. By using codelab, you are able to run python commands without using terminal.

### Challenge 4
The average and standard deviation of the grain sizes in the grain.png file. Average and Standard Deviation will print as (Avg, Std Dev) after the command runs.

The amount of DNA origami in the dna.tif file. Running the command $ python dnacounter.py  will print the dna.tif file with isolated DNA origami and print the number below. 


# Who do I talk to? #

### Repo owner or admin ###

* Eric Jankowski: ericjankowski@boisestate.edu

#### Other community or team contact ####

* Kendra Noneman: kendranoneman@u.boisestate.edu
* John Page: johnpage@u.boisestate.edu
* Matt Lawson: matthewlawson@u.boisestate.edu
* Julie Pipkin: juliepipkin@u.boisestate.edu
* Kody King: kodyking@u.boisestate.edu
* Kiev Dixon: kievdixon@u.boisestate.edu
* Nate Farris: natefarris@u.boisestate.edu
