from PIL import Image
import numpy as np
import matplotlib.pyplot as plt

i = 0 #grain counter

pixthresh = 140

image = Image.open('grain.png')
image = image.convert("L")

imageData = np.asarray(image)
thresholdData = (imageData > pixthresh) * 1.0

#plt.imshow(thresholdData)
#plt.show()

xprime = 0

print(thresholdData.shape)

for x in thresholdData[200]:
	if x != xprime:
		i = i + 1
		xprime = x

print("Number of grains:",i/2) 

# x direction is 1438.9 um
# y direction is 1069.4 um
# 1.44 pixels per um
